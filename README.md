# Example Python password key/hash generation
160-bit, 2048 iteration PBKDF2 key derivation function is used to create the
password hash.

## Prerequisites
* Python

## Download source
```sh
git clone https://gitlab.com/ntnu-idri2004/password
```

## Run
```sh
cd password
python password.py
```
